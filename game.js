var game_window_width =  650;
var game_window_heigth = 500;
var totallife = 0;
var enemyamount = 0; 
var remainlife;
var score = 0;
var scoreText;
var world_y = 8000;

var isleft=0;


var weapon ;
var weapon2 ;
var enemys ; 
var enemyBullet ;
var stars;
var aims;
var menustars;
var livingEnemies = [];
var enemylastfiretime=0;
var enemynowtime = 0;
var pass = 0;
var skilling = 0;
var skilltimecount = 0;
var skillimage;
var lastskillscore = 0;
var red;
var level;
var enemybulletspeed;
var shootsound;
var shootsoundcount=0;
var boom;
var bgm;
var currentvolume = 5;
var volumetext ;
var weapontype = 0;
var weapontypecount = 0;
var bullet2images;
var littlehelper;
var littlehelperbullets;
var littlehelperweapon;
var trilittlehelpers;

var explosions;
var hashelp = 0;

var boss;
var bosss;


var menuState = {
    preload: function(){
        game.stage.backgroundColor = '#000000';
       // game.load.image('menustar', 'assets/star.png');
       game.load.bitmapFont('carrier_command', 'assets/carrier_command.png', 'assets/carrier_command.xml');
       game.load.image('title', 'assets/title.png');
       game.load.spritesheet('button', 'assets/button.png', 80, 20);
       game.load.audio('bgm', 'assets/backgroundmusic.mp3');

    },
    create: function(){
        /*menustars = game.add.group();
        for (var i = 0; i < 30; i++){
            menustars.create(game_window_width, game_window_heigth, 'menustar');
        }*/
        score = 0;

        var playText = game.add.bitmapText(200, 300, 'carrier_command','Choose Level',15);
        game.add.image(70,50,'title');
        game.add.bitmapText(250, 350, 'carrier_command','1',15);
        game.add.bitmapText(300, 350, 'carrier_command','2',15);
        game.add.bitmapText(350, 350, 'carrier_command','3',15);
        this.starttoplay1= this.input.keyboard.addKey(Phaser.KeyCode.ONE);
        this.starttoplay2= this.input.keyboard.addKey(Phaser.KeyCode.TWO);
        this.starttoplay3= this.input.keyboard.addKey(Phaser.KeyCode.THREE);

        var buttonup = game.add.button(150, 450, 'button', clickup, this, 0, 1, 2);
        var textup = game.add.bitmapText(150, 450 + 4, 'carrier_command', "+", 10);
        textup.x += (buttonup.width / 2) - (textup.textWidth / 2);
        textup.tint = '#000000';
        var buttondown = game.add.button(150, 475, 'button', clickdown, this, 0, 1, 2);
        var textdown = game.add.bitmapText(150, 475 + 4, 'carrier_command', "-", 10);
        textdown.x += (buttondown.width / 2) - (textdown.textWidth / 2);
        textdown.tint = '#000000';
        volumetext = game.add.bitmapText(20, 460, 'carrier_command','Volume: ' + currentvolume ,10)

        bgm = game.add.audio('bgm');
        bgm.volume = 1;
        bgm.play();

    },
    update: function(){
        bgm.volume = 0.5 + currentvolume/10;
        volumetext.text = 'Volume: ' + currentvolume;
        if(this.starttoplay1.isDown) {
            level = 1;
            game.state.start('main');
        }else if(this.starttoplay2.isDown){
            level = 2;
            game.state.start('main');
        }else if(this.starttoplay3.isDown){
            level = 3;
            game.state.start('boss');
        }
    }
}

var mainState = {
    preload: function() {     

        game.load.spritesheet('player', 'assets/plane4.png', 35, 60);
        //game.load.spritesheet('player', 'assets/plane3.png', 35, 58);

        game.load.image('mushroom', 'assets/enemy.png');
        game.load.image('red', 'assets/red.png');
        game.load.spritesheet('bullet', 'assets/rgblaser.png', 4, 4);
        game.load.image('enemyBullet', 'assets/enemy-bullet.png');
        game.load.image('skillimage', 'assets/skill.png');
        game.load.image('health', 'assets/health.png');
        game.load.bitmapFont('carrier_command', 'assets/carrier_command.png', 'assets/carrier_command.xml');
        game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
        game.load.image('star', 'assets/star.png');
        game.load.audio('shootsound', 'assets/shoot.wav');
        game.load.audio('boom', 'assets/boom.mp3');
//        game.load.audio('bgm', 'assets/backgroundmusic.mp3');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('aim', 'assets/aim.png');
        game.load.image('bullet2', 'assets/bullet2.png');
        game.load.image('bullet2image', 'assets/bullet2image.png');
        game.load.image('littlehelper', 'assets/戰鬥機2.png');
        game.load.image('littlehelperbullets', 'assets/littlehelperbullets.png');
        game.load.image('trilittlehelpers', 'assets/trilittlehelper.png');
    },
    create: function() {

        
        //this.enemy = new Array(50);
        game.world.setBounds(0, 0, 650, world_y);
        game.stage.backgroundColor = '#000000';
        shootsound = game.add.audio('shootsound');
        boom = game.add.audio('boom');
//        bgm = game.add.audio('bgm');
        stars = game.add.group();

//        bgm.play();

        hashelp =0;
        
        for (var i = 0; i < 128; i++){
            stars.create(game.world.randomX, game.world.randomY, 'star');
        }

        aims = game.add.group();
        aims.enableBody = true;
        aims.physicsBodyType = Phaser.Physics.ARCADE;
        for(var w=0; w<3;w++){
            var aim = aims.create(game.world.randomX, game.world.randomY, 'aim');
        }

        bullet2images = game.add.group();
        bullet2images.enableBody = true;
        bullet2images.physicsBodyType = Phaser.Physics.ARCADE;
        for( w=0; w<3;w++){
            var  bullet2image = bullet2images.create(game.world.randomX, game.world.randomY, 'bullet2image');
        }



        this.health = new Array(totallife);
        
        pass = 0;

        if(level == 1){
            totallife = 3;
            enemyamount = 150;
            enemybulletspeed = 120;
        }
        else if(level == 2) {
            totallife = 1;
            enemyamount = 250;
            enemybulletspeed = 360;
        }

        remainlife = totallife;

        weapontype = 0;
        score = 0;
        skilling = 0;
        lastskillscore = 0;

        for(var j = 0; j<totallife ; j++)
        {
            this.health[j] = game.add.sprite(game_window_width-30-20*j,20,'health');
            this.health[j].fixedToCamera = true;
        }

        skillimage = game.add.image(20,40,'skillimage');
        skillimage.fixedToCamera = true;
        skillimage.visible = false;

        red = game.add.image(0,0,'red');
        red.visible = false;
        red.fixedToCamera = true;

        
    
        scoreText = game.add.bitmapText(20,20, 'carrier_command','Score '+ score,10);
        scoreText.fixedToCamera = true;
        game.camera.x = 100;
        game.camera.y = world_y;

        game.physics.startSystem(Phaser.Physics.ARCADE);

        enemys = game.add.group();
        enemys.enableBody = true;
        enemys.physicsBodyType = Phaser.Physics.ARCADE;

        for (var i = 0; i < enemyamount; i++){
            var enemy = enemys.create(getRandom(0,game_window_width-20), game.world.randomY-game_window_heigth , 'mushroom');            
        }

        enemyBullets = game.add.group();
        enemyBullets.enableBody = true;
        enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        enemyBullets.createMultiple(2000, 'enemyBullet');

        trilittlehelpers = game.add.group();
        trilittlehelpers.enableBody = true;
        trilittlehelpers.physicsBodyType = Phaser.Physics.ARCADE;
        for(i = 0; i<2;i++){
            var trilittlehelper = trilittlehelpers.create(getRandom(0,game_window_width-200), game.world.randomY-game_window_heigth , 'trilittlehelpers');
        }
/*
        trilittlehelpers = game.add.sprite(200,7400,'trilittlehelpers');
        trilittlehelpers.enableBody = true;
        trilittlehelpers.physicsBodyType = Phaser.Physics.ARCADE;
*/

        weapon = game.add.weapon(2000, 'bullet');
        weapon.setBulletFrames(0, 80, true);
        weapon.bulletKillType = Phaser.Weapon.KILL_CAMERA_BOUNDS;
        weapon.bulletSpeed = 400;
        weapon.fireRate = 240;

        
        weapon2 = game.add.weapon(2000, 'bullet2');
        weapon2.bulletKillType = Phaser.Weapon.KILL_CAMERA_BOUNDS;
        weapon2.bulletSpeed = 800;
        weapon2.fireRate = 120;

        littlehelperweapon = game.add.weapon(2000, 'littlehelperbullets');
        littlehelperweapon.bulletKillType = Phaser.Weapon.KILL_CAMERA_BOUNDS;
        littlehelperweapon.bulletSpeed = 800;
        littlehelperweapon.fireRate = 120;

        ///

        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();

        
        this.player = game.add.sprite(game.width/2, world_y - 200, 'player');
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds=true;

        this.player.animations.add('leftwalk', [1], 8, true);
        this.player.animations.add('rightwalk', [2], 8, true);
        this.player.animations.add('straightwalk', [0], 8, true);
        this.player.animations.add('planeskill', [3], 8, true);

        littlehelper = game.add.image(this.player.x-20,this.player.y-20,'littlehelper');
        littlehelper.visible = false;

        weapon.trackSprite(this.player, 25, 0,);
        weapon2.trackSprite(this.player, 20, 0,);
        littlehelperweapon.trackSprite(littlehelper, 5, 0,);
        fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        //autoaim = this.input.keyboard.addKey(Phaser.KeyCode.S);
        skill = this.input.keyboard.addKey(Phaser.KeyCode.A);

        game.physics.arcade.enable(this.player);


        explosions = game.add.group();
        explosions.createMultiple(enemyamount, 'kaboom');
        explosions.forEach(setupexplo, this);


        
        game.input.onDown.add(unpause, self);

        choiseLabel = game.add.text(160, 250, 'Click screen to continue', { font: '30px Arial', fill: '#fff' });
        choiseLabel.visible = false;
        choiseLabel.fixedToCamera = true;

        function unpause(event){
            if(game.paused){
                game.paused = false;
                choiseLabel.visible = false;
            }
        }

        pause_label = game.add.text(600,480 , 'Pause', { font: '15px Arial', fill: '#fff' });
        pause_label.inputEnabled = true;
        pause_label.fixedToCamera = true;
        pause_label.visible = true;
        pause_label.events.onInputUp.add(function () {
        // When the paus button is pressed, we pause the game
            game.paused = true;
            choiseLabel.visible = true;
        });

        //movetri(trilittlehelpers);


    },
    enemyFires: function() {

        //  Grab the first bullet we can from the pool
        enemyBullet = enemyBullets.getFirstExists(false);
        
    

        livingEnemies.length=0;
    
        enemys.forEachAlive(function(enemy){
    
            // put every living enemy in an array

            if(enemy.y > game.camera.y) livingEnemies.push(enemy);

            if(enemy.y > game.camera.y + 250 )livingEnemies.pop(enemy);
            //else if(!enemy.inWorld) livingEnemies.pop(enemy);

        });
        enemynowtime ++;
    
        if (enemyBullet && livingEnemies.length > 0 && enemynowtime - 30/level > enemylastfiretime)
        {
            
            var random=game.rnd.integerInRange(0,livingEnemies.length-1);
    
            // randomly select one of them
            var shooter=livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x+20, shooter.body.y+15);
            enemylastfiretime = game.time.now;
            enemynowtime = enemylastfiretime;
    
            game.physics.arcade.moveToObject(enemyBullet,this.player,enemybulletspeed);
            
        }
    
    },
    enemymove: function(){
        var decidetomove = getRandom(1,200);

        if(livingEnemies.length > 0){
            var random = game.rnd.integerInRange(0,livingEnemies.length-1);
            var mover=livingEnemies[random];
            var dis = getRandom(50,100);
            if(decidetomove >= 195 ){
                var Topoint = new Phaser.Point(mover.x+dis,mover.y+10);
                game.physics.arcade.moveToObject(mover,Topoint,100);
            }
            else if(decidetomove >= 190){
                var Topoint = new Phaser.Point(mover.x-dis,mover.y+10);
                game.physics.arcade.moveToObject(mover,Topoint,100);
            }
            else{
                for(var i=0;i<livingEnemies;i++)
                    livingEnemies[i].animations.stop();
            }
        }
    },
    update: function() {    

        red.visible = false ;

        //if(trilittlehelpers.y  == game.camera.y ) movetri(trilittlehelpers);

        game.physics.arcade.overlap(aims, this.player,changeweapon);
        game.physics.arcade.overlap(enemys, weapon.bullets,killThem);
        game.physics.arcade.overlap(enemys, weapon2.bullets,killThem);
        if(hashelp ==1)game.physics.arcade.overlap(enemys, littlehelperweapon.bullets,killThem);
        game.physics.arcade.overlap(bullet2images, this.player, changebullet2);
        game.physics.arcade.overlap(trilittlehelpers, this.player, trihelp);
        //game.physics.arcade.overlap(enemys, weapon.bullets,changeweapon);

        if(isleft==0)littlehelper.x = this.player.x-5; 
        else littlehelper.x = this.player.x-15; 
        littlehelper.y = this.player.y-5;
        
        if(skill.isDown && skillimage.visible == true) playskill();

        if(skilling == 0){
            game.camera.y -= 1;
            game.physics.arcade.overlap(enemyBullets, this.player,enemyhitplayer);
            /*if(autoaim.isDown){
                if(livingEnemies.length > 0){
                    var random = game.rnd.integerInRange(0,livingEnemies.length-1);
                    var aimed=livingEnemies[random];
                    weapon.bulletSpeed = 800;
                    weapon.fireAtXY(aimed.x,aimed.y+20);
                }
            }*/

            if(weapontype!=0) weapontypecount ++;

            if (fireButton.isDown){ 

                if(weapontype == 0){
                    shootsoundcount ++;  
                    if(shootsoundcount > 10) {
                        shootsoundcount = 0;
                        shootsound.volume = 0.5;
                        shootsound.play();
                    } 
                    weapon.fire();
                    weapon.bulletSpeed = 400;
                }else if(weapontype ==1){
                    if(livingEnemies.length > 0){
                        shootsoundcount ++;  
                        
                        if(shootsoundcount > 10) {
                            shootsoundcount = 0;
                            shootsound.volume = 0.5;
                            shootsound.play();
                        } 
                        var random = game.rnd.integerInRange(0,livingEnemies.length-1);
                        var aimed=livingEnemies[random];
                        weapon.bulletSpeed = 800;
                        weapon.fireAtXY(aimed.x,aimed.y+20);
                        if(weapontypecount > 480) {
                            weapontypecount = 0;
                            weapontype = 0;
                        }
                    }
                }
                else if(weapontype == 2){
                    weapon2.fire();
                    if(weapontypecount > 480) {
                        weapontypecount = 0;
                        weapontype = 0;
                    }
                }


            }
        }else {
            game.camera.y -= 10;
            skilltimecount ++;
            game.physics.arcade.overlap(enemys, this.player,crashenemy);
        }

        if(score%200 == 0 && score!=lastskillscore){
            lastskillscore = score;
            skillimage.visible = true;
        }

        if(skilltimecount > 120){
            skilltimecount=0;
            skilling = 0;
            this.player.animations.play('straightwalk'); 
        }
        if (remainlife == 0 ) { this.playerDie();}

        
        if(hashelp==1 && skilling == 0)littlehelperweapon.fire();

        this.movePlayer();
        this.enemyFires();
        this.enemymove();

        if(game.camera.y == 0){
            if(this.player.y <10){
                game.state.start('gameover');
                pass = 1;
            }
        }
    }, 
    playerDie: function() { 
        pass = 0;
        game.state.start('gameover');
    },
    movePlayer: function() {

        var worldup = game.camera.y;
        var worlddown = game.camera.y + 500;
        var worldleft  = game.camera.x;
        var worldright = game.camera.x + 650;
        if (this.cursor.left.isDown) {
            if(this.player.x > worldleft) this.player.body.velocity.x = -400;
            else this.player.body.velocity.x = 0;

            if(skilling == 1) this.player.body.velocity.y = -600;
            else {
                this.player.body.velocity.y = -60; 
                this.player.animations.play('leftwalk');
                isleft = 1;
            }
             
        }
        else if (this.cursor.right.isDown) { 
            isleft = 0;
            if(this.player.x + 50 < worldright) this.player.body.velocity.x = 400;
            else this.player.body.velocity.x = 0;
            this.player.body.velocity.y = -60;

            if(skilling == 1) this.player.body.velocity.y = -600;
            else {
                this.player.body.velocity.y = -60;
                this.player.animations.play('rightwalk');
            }
 
        }    
        else if (this.cursor.up.isDown &&skilling != 1) { 
            isleft = 0;
            if(this.player.y > worldup) this.player.body.velocity.y = -400;
            else this.player.body.velocity.y = 0;
            this.player.animations.play('straightwalk'); 
        }  
        else if(this.cursor.down.isDown &&skilling != 1){
            isleft = 0;
            if(this.player.y + 50 < worlddown) this.player.body.velocity.y = 400;
            else this.player.body.velocity.y = -60;
            this.player.animations.play('straightwalk'); 
        }
        else {
            isleft = 0;
            this.player.body.velocity.x = 0;

            if(skilling == 1){
                this.player.body.velocity.y = -600;
                this.player.animations.play('planeskill'); 
            } 
            else {
                this.player.body.velocity.y = -60;
                this.player.animations.play('straightwalk'); 
            }
            
            if(skilling == 0)this.player.animations.stop();
        }    
    }
};

var bossState = {
    preload: function(){
        game.load.spritesheet('player', 'assets/plane4.png', 35, 60);
        game.load.image('bullet2', 'assets/bullet2.png');
        game.load.image('boss', 'assets/boss.png');
        game.load.image('bullet2', 'assets/bullet2.png');
        game.load.image('enemyBullet', 'assets/enemy-bullet.png');
        game.load.image('health', 'assets/health.png');
        game.load.image('red', 'assets/red.png');
        game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
        game.load.audio('shootsound', 'assets/shoot.wav');
    },
    create: function(){
        game.world.setBounds(0, 0, 650, 500);
        shootsound = game.add.audio('shootsound');
        this.cursor = game.input.keyboard.createCursorKeys();
        this.bossblood = 50;


        red = game.add.image(0,0,'red');
        red.visible = false;

        //game.add.image(100,100,'boss')

        this.health = new Array(3);

        this.player = game.add.sprite(game.width/2,  500- 200, 'player');
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds=true;
        this.player.animations.add('leftwalk', [1], 8, true);
        this.player.animations.add('rightwalk', [2], 8, true);
        this.player.animations.add('straightwalk', [0], 8, true);
        this.player.animations.add('planeskill', [3], 8, true);

        this.bossfiretime = 0;

        remainlife = 3;
        for(var j = 0; j<3 ; j++)       {
            this.health[j] = game.add.sprite(game_window_width-30-20*j,20,'health');
        }

        enemyBullets = game.add.group();
        enemyBullets.enableBody = true;
        enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        enemyBullets.createMultiple(2000, 'enemyBullet');

        fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);

        weapon2 = game.add.weapon(2000, 'bullet2');
        weapon2.bulletKillType = Phaser.Weapon.KILL_CAMERA_BOUNDS;
        weapon2.bulletSpeed = 800;
        weapon2.fireRate = 120;

        weapon2.trackSprite(this.player, 20, 0,);

        bosss = game.add.group();
        bosss.enableBody = true;
        bosss.physicsBodyType = Phaser.Physics.ARCADE;

        for (var i = 0; i < 1; i++){
            boss = bosss.create(0, 100, 'boss');    
            boss.body.collideWorldBounds=true;        
        }

    },
    bossmove: function(){
        var decidetomove = game.rnd.integerInRange(0,200);
        if(boss.x == 0 ){
            var Topoint = new Phaser.Point(boss.x+200,boss.y);
            game.physics.arcade.moveToObject(boss,Topoint,100);
        }else if(boss.x > 400){
            var Topoint = new Phaser.Point(0,boss.y);
            game.physics.arcade.moveToObject(boss,Topoint,100);
        }

    },
    enemyfire: function(){
        enemyBullet = enemyBullets.getFirstExists(false);
        enemyBullet.reset(boss.body.x+100, boss.body.y+70);
        game.physics.arcade.moveToObject(enemyBullet,this.player,200);
    },
    movePlayer: function() {

        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -400;
            this.player.animations.play('leftwalk');    
            this.player.body.velocity.y = 0;        
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 400;
            this.player.body.velocity.y = 0;
            this.player.animations.play('rightwalk');
        }    
        else if (this.cursor.up.isDown &&skilling != 1) { 
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = -400;
            this.player.animations.play('straightwalk'); 
        }  
        else if(this.cursor.down.isDown &&skilling != 1){
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 400;
            this.player.animations.play('straightwalk'); 
        }
        else {
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
            this.player.animations.play('straightwalk');            
        }    
    },
    playerDie: function() { 
        pass = 0;
        game.state.start('gameover');
    },
    enemyhitplayer: function(play,bullet){
        bullet.kill();
    
        red.visible = true;
    /*
        var emitter = game.add.emitter(this.player.x, this.player.y, 15);
        emitter.makeParticles('pixel');
        emitter.setYSpeed(-150, 150);
        emitter.setXSpeed(-150, 150);
        emitter.setScale(2, 0, 2, 0, 800);
        emitter.gravity = 500;
        emitter.start(true, 800, null, 15);
    */
        if(remainlife > 0){
            bossState.health[remainlife-1].kill();
            remainlife --;
        }
    },
    update: function(){
        red.visible = false;
        this.movePlayer();
        if (fireButton.isDown){ 
            shootsoundcount ++;
            if(shootsoundcount > 10) {
                shootsoundcount = 0;
                shootsound.volume = 0.5;
                shootsound.play();
            } 
            weapon2.fire();
        }
        this.bossmove();
        this.bossfiretime++;
        if(this.bossfiretime>30){
            this.enemyfire();
            this.bossfiretime = 0;
        }
        game.physics.arcade.overlap(enemyBullets, this.player,this.enemyhitplayer);
        game.physics.arcade.overlap(bosss, weapon2.bullets,killboss);
        if (remainlife == 0 ) { this.playerDie();}
        if(this.bossblood < 0) {
            score = 1000;
            pass = 1;
            game.state.start('gameover');
        }
    }
}


var gameoverState = {
    preload: function(){
        game.load.image('corona', 'assets/blue.png');
        game.load.bitmapFont('carrier_command', 'assets/carrier_command.png', 'assets/carrier_command.xml');

    },
    create: function(){
        game.world.setBounds(0, 0, 650, world_y);
        game.stage.backgroundColor = '#000000';

        bgm.stop();
        emitter = game.add.emitter(game.world.centerX, 300, 200);
    
        emitter.makeParticles('corona');
    
        emitter.setRotation(0, 0);
        emitter.setAlpha(0.3, 0.8);
        emitter.setScale(0.5, 1);
        emitter.gravity = -200;
        emitter.start(false, 5000, 100);
        if(pass == 0)var bmpText = game.add.bitmapText(220, 220, 'carrier_command','You Lose',20);
        else var bmpText = game.add.bitmapText(230, 220, 'carrier_command','You Win',20);

        if(score == 0) var finalscoreText = game.add.bitmapText(260, 270, 'carrier_command','Score: ' + score,10);
        else if(score < 100) var finalscoreText = game.add.bitmapText(255, 270, 'carrier_command','Score: ' + score,10);
        else var finalscoreText = game.add.bitmapText(250, 270, 'carrier_command','Score: ' + score,10);

        var playagaintext = game.add.bitmapText(90, 400, 'carrier_command','Click Enter to play again',15);
        this.playagain = this.input.keyboard.addKey(Phaser.KeyCode.ENTER);

    },
    update: function(){
        if(this.playagain.isDown) game.state.start('menu');
    }
}

var game = new Phaser.Game(game_window_width , game_window_heigth , Phaser.AUTO, 'canvas');
game.state.add('menu', menuState);
game.state.add('main', mainState);
game.state.add('boss', bossState);
game.state.add('gameover', gameoverState);
//game.state.start('boss');
game.state.start('menu');
//game.state.start('main');
//pass =1;
//game.state.start('gameover');

function killThem(object1,object2){

    if(object1.y > game.camera.y-20){
        var explosion = explosions.getFirstExists(false);
        explosion.reset(object1.body.x-50, object1.body.y-50);
        explosion.play('kaboom', enemyamount, false, true);

        object1.kill();
        object2.kill();
        score += 10;
        scoreText.text = 'Score '+ score;
    }
}

function killboss(object1, object2){
    object2.kill();
    bossState.bossblood --;
}

function enemyhitplayer(player,bullet){
    bullet.kill();

    red.visible = true;

    var emitter = game.add.emitter(mainState.player.x, mainState.player.y, 15);
    emitter.makeParticles('pixel');
    emitter.setYSpeed(-150, 150);
    emitter.setXSpeed(-150, 150);
    emitter.setScale(2, 0, 2, 0, 800);
    emitter.gravity = 500;
    emitter.start(true, 800, null, 15);

    if(remainlife > 0){
        mainState.health[remainlife-1].kill();
        remainlife --;
    }
}

function crashenemy(player,enemy){
    var explosion = explosions.getFirstExists(false);
    explosion.reset(enemy.x-50, enemy.y-50);
    explosion.play('kaboom', enemyamount, false, true);
    enemy.kill();
    score += 10;
    scoreText.text = 'Score '+ score;
}

function getRandom(min,max){
    return Math.floor(Math.random()*(max-min+1))+min;
};

function setupexplo (invader) {
    invader.animations.add('kaboom');
}

function playskill(){
    skilling = 1;
    skillimage.visible = false;
    boom.play();
    mainState.player.animations.play('planeskill'); 
}

function clickup(){
    if(currentvolume!= 10)
        currentvolume += 1;
}

function clickdown(){
    if(currentvolume!= 1)
        currentvolume -= 1;
}

function changeweapon(player,aim){
    weapontypecount = 0;
    aim.kill();
    weapontype = 1;
}

function changebullet2(player,aim){
    weapontypecount = 0;
    aim.kill();
    weapontype = 2;
}



function trihelp(player,tri){
    tri.kill();
    hashelp = 1;
    littlehelper.visible = true;
}











