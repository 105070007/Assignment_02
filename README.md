# Software Studio 2019 Spring Assignment 2

## Basic Components
|Component|Score|Y/N| Y
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description

遊戲說明：
    一進到選單畫面，可按 123 鍵選擇關卡（1 為簡單關卡，2 為困難關卡，3為 Boss 關卡）
    上下左右鍵控制戰績移動
    空白鍵可讓 player 往前發射子彈攻擊敵人
    A 鍵可釋放技能（每得 200 分可釋放一次技能，當可以釋放技能時，畫面左上會出現“Boom"圖示）

# Basic Components Description : 
1. Jucify mechanisms : 
    level:
        遊戲剛開始有三個困難度可供玩家選擇
        關卡2的敵人子彈速度和敵人數都明顯較關卡1多，關卡三為 Boss 關卡。
    skill:
        A 鍵可釋放技能（每得 200 分可釋放一次技能，當可以釋放技能時，畫面左上會出現“Boom"圖示）
        技能為戰鬥機向前衝刺，衝刺期間會是無敵狀態，且可摧毀被撞到的敵機。

2. Animations : [xxxx]
    player 往左右移動飛行時，會有不同的動畫

3. Particle Systems : [xxxx]
    player被敵人攻擊時，身上會有 Particle Systems 的效果（被打出血塊！）

4. Sound effects : [xxxx]
    一進到遊戲選單就會有背景音，且背景音可透過點選左下角的 + 和 - 調整背景音大小
    player 攻擊時會有音效
    player 釋放技能期間也會有音效

5. UI:
    Player health 第1和第3關玩家有三條血，第二關玩家只有一條血，血量會顯示在右上角
    score : 分數會顯示在畫面左上方，並會在遊戲結束時告知玩家其總分
    Game pause : 按右下角的 "pause" 可暫停，再按畫面任一處就可繼續遊戲

# Bonus Functions Description : 
1. Bullet automatic aiming : 在遊戲中會隨機出現 "標靶"，若吃到標靶則玩家會有五秒的時間子彈會自動瞄準敵人
2. Unique bullet : 在遊戲中會隨機出現 "黃光"，若吃到黃光則玩家有五秒的時間可射出速度較快且攻擊速度較快的子彈
3. Little helper :  在遊戲中會隨機出現 "戰機"，若玩家吃到戰機則會有 little helpe 在一旁協助玩家攻擊敵人
4. Boss: 第三關為 Boss 關卡，玩家進入關卡後會遇到攻擊速度跟頻率極快的 UFO ，要射到 UFO 50 次才會將該 Boss 擊倒
5. 敵機摧毀動畫，敵人被璀毀時會有動畫 
6. 玩家受傷時，畫面會閃紅